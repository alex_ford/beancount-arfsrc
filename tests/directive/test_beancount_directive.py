#!/usr/env python3

import pytest
import sys
sys.path.append("../../")
from beancount_arfsrc.directive import BeancountDirective
import logging

logging.basicConfig(level=logging.DEBUG)
_log = logging.getLogger(__name__)

def test_construction():
    bcd = BeancountDirective()
    _log.info(f"BeancountDirective: $(bcd)")

