# Beancount ARF Source module/library

`beancount-arfsrc`

This mini-project is based on the concepts created in the [Beancount](http://furius.ca/beancount/) double-entry, text-based, CLI accounting project.

This is a library of python code which I have written to support generalized concepts/library features that are used by my other python scripts in my own personal ecosystem of Beancount-related mini-projects.

I do **not** really recommend others to use this over the actual Beancount source, as lots of this is redundant with the official Beancount code, but I use it here because it does represent a somewhat simplified version of the Beancount Object Model at the expense of various features. As a "library" module, there isn't anything here that would be directly useful to an end-user attempting to get financial data into their Beancount data file. (But it is required by my other mini-projects.)

**Library Maturity**: Beta

**Author**: Alex Richard Ford (arf4188@gmail.com)

**Website**: http://www.alexrichardford.com

**License**: MIT License (see [LICENSE](LICENSE) file for more details)

## Dependencies

In addition to this Git repo, you will also need the following other repos:

* None

## Quick Start Usage

```shell
pipenv install
pipenv run build
```

## Quick Start Development

I recommend using Visual Studio Code, which then you can simply use the included `beancount-arfsrc.code-workspace` file.

To test the code, simply execute the following:

```shell
pipenv install --dev
pipenv run pytest -s
 # or
pipenv run tests
```

## Common Conventions

In all of my beancount-* mini-project repositories, I follow the following conventions:

* Beancount data files (with your actual transactions in them) use the `.beancount` extension
* Beancount spec files (which are supporting files used by my scripts and plugins) use the `.bcspec` extension

