"""
BeancountObjectBuffer is a class that represents exactly 1 or more
text lines in the BeancountFile that are grouped together in order
to represent some logical object in the Beancount system.

It also stores some useful metadata about the lines which are useful
for in-place updates of the data file later on.

@author Alex Ford (arf4188@gmail.com)
"""

class BeancountObjectBuffer():
    
    def __init__(self):
        pass
