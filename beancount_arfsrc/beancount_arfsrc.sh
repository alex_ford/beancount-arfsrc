#!/bin/bash

### Primary Bash script for inclusion in other Bash scripts
### supporting Beancount ARF Projects.
###
### Usage:
###    source ~/Workspaces/Personal/beancount/beancount-arfsrc/beancount_arfsrc/beancount_arfsrc.sh
###
### Author: Alex Richard Ford <arf4188@gmail.com>

function bcas_version() {
    echo "Beancount ARFSRC v1.0.0"
}

function debug() {

}

function info() {

}

function warn() {

}

function error() {
    
}

function fatal() {
    
}
