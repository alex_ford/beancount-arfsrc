#!/usr/env python3

"""
MATURITY: Not Yet Usable!
This is the parent object for an entire Beancount data file.
It contains all of the data, represented as Python objects,
in the file. It has the ability to read and write this data
to the filesystem so it can be used by other Beancount-compatible
software.

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License (see LICENSE)
"""

"""
# FEATURES/TODOs
- [o] Read/parsing of beancount transaction (entries) and their
       associated postings and metadata.
- [] Handling of other included beancount files.
        - This can be modeled with a "recursive" strategy, using the BeancountFile
        class for each discreet file, storing them in their parent BeancountFile
        object using a simple list structure. Each item in the list is a tuple
        that describes (filepath, BeancountFile object)
- [] Write Beancount data represented by this BeancountFile to local file(s).
- [] Main method for direct usage/testing of BeancountFile.
        - When used as a main entry point, the user can supply the file to load,
        along with a command that maps to one of the methods/operations that can
        be performed on this object.
- [] Provides an aggregated iterator of all directives represented by this BeancountFile
- [] Provides an iterator to loop through each 'transaction', along with perhaps
    other filters to just work with the different types of directives.
- Lookup mechanisms
    - The following are provided by this class which return
      a List of 0 or more BeancountDirectives.
        - [] Lookup by date.
        - [] Lookup by date range.
        - [] Lookup by payee field.
        - [] Lookup by account field.
- [] Comparison of two BeancountFiles
"""

import argparse
import re
import io

from beancount_arfsrc.beancount_object_buffer import BeancountObjectBuffer

class BeancountFile(object):
    """Represents a single file which contains Beancount data.

    This makes no assumptions about whether or not there are multiple related 
    Beancount data files, just that this *is* one. It can handle the Beancount
    'include' keyword, which nests Beancount data files in one another.
    """

    directives: BeancountDirective[] = []

    # TODO: why does this have a default of "" for the file?
    def __init__(self, bcFile: io.TextIOWrapper):
        self.bcFile = bcFile
        self.readFromFile()


    def readFromFile(self):
        """Read entries from the file specified when this was constructed."""
        with self.bcFile as f:
            # Read every line from the data file into a small collection
            # buffer - this represents 1 or more lines belonging to a single
            # logical object in Beancount
            bcObjectBuffer = BeancountObjectBuffer()
            for line in f:

                # TODO move everything below here into code that operates
                    # over bcObjectBuffer and not on the lines themselves.
                    # Ignore comments, for now
                if line.startswith(";"):
                    continue
                # Ignore options, for now
                if line.startswith("option"):
                    continue
                # Ignore newlines
                if line.startswith("\n"):
                    continue
                
                # Line starts with 4 digits, followed by whatever else
                # This is the start of a directive/entry
                if re.match("[\\d]{4}", line):
                    tokens = line.split(" ")
                    dateStr = tokens[0]
                    
                    # Account open directive - ignore for now
                    if re.match("[\\d]{4}.*open.*", tokens[1]):
                        continue

                    # Entry directive, example form:
                    """
                    YYYY-MM-DD flag "payee"   "narration"   [#tags...] [^links...]
                      [meta_key: "meta_value"]
                      [flag] Account:One          Value Commodity [CostSpec]
                        [meta_key: "meta_value"]
                      [flag] Account:Two          Value Commodity [CostSpec]
                        [meta_key: "meta_value"]
                    """
                    if BeancountEntry.isEntry(lines)
                    if re.match("[!][*]", tokens[1]):
                        flagStr = tokens[1]
                        continue
                
                # TEST/DEV: this prints out the next line that wasn't handled by the above
                print("UNHANDLED LINE: \"{}\"".format(line))
                break
    
    
    def writeToFile(self, destFile=None):
        """ Write out this BeancountFile to a local file.

        If destFile is left as None, this will write out to the same file that
        was used as the input file. If setEffectiveDateRange() was used to set
        the start and end date range, then this method will only output those
        entries that fall within the range.
        """
        pass
    
    
    def getEntriesForDate(self, date):
        """ Retrieves the entries for a single date.
        
        This ignores the effects of the setEffectiveDateRange() method.
        """
        pass
    
    
    def getEntriesForDateRange(self, startDate, endDate):
        """ Retrieves the entries for the specified start and end date range.
        
        This ignores the effects of the setEffectiveDateRange() method.
        """
        pass 
    
    
    def setEffectiveDateRange(self, startDate, endDate):
        """ Place a start and end date range on the transactions that this 
        BeancountFile will output. """
        pass

    
    def sort(self):
        """ This reorganizes the entries so that they are sorted in ascending
        order, that is, so that earlier dates come before later dates. """
        pass


    def toBeancountStr(self):
        return "BeancountFile: {}".format(self.bcFile)
    
    def __str__(self):
        return self.toBeancountStr()
    def __repr__(self):
        return self.toBeancountStr()
    

    def __iter__(self):
        return self.directives


def main():
    argParser = argparse.ArgumentParser(
        description="Provides some of the functionality of BeancountFile as a "
                    "CLI tool."
    )
    argParser.add_argument("bcFile", 
        help="The beancount data file to read.",
        type=argparse.FileType("r")
    )
    args = argParser.parse_args()

    bcf = BeancountFile(args.bcFile)
    print(bcf)

if __name__ == "__main__":
    main()
