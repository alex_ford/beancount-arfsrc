#!/usr/env python3

""" Beancount Directive.

This file implements the abstract base class for all fully-qualified entries
in a beancount data file.

See 'beancount_transaction.py' for one of the subclasses.

All Directives essentially start with a date, followed by additional fields
depending on the exact type of Directive.

Author: Alex Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License
"""

import arrow

class BeancountDirective():

    _date: arrow.Arrow = None

    def __init__(self, date=arrow.now()):
        self._date = date
    

    def toBeancountStr(self) -> str:
        """ Serializes this BeancountDirective to the string representation. """
        return f"{self._date.datetime.year}-{self._date.datetime.month:02}-{self._date.datetime.day:02}"
