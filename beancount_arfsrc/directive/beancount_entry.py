#!/usr/env python3

# TODO: make use of std logging module; replace appropriate echo/output statements

# TODO: replace by arrow
import datetime

from beancount_arfsrc.directive.parts import BeancountPosting
from beancount_arfsrc.directive.parts import BeancountMetadata

# TODO: this should extend the BeancountDirective super class.
class BeancountEntry(object):
    """
    Represents a beancount entry object; this should probably be broken up further
    into BeancountPostings (which make up BeancountEntries).
    """

    def __init__(self):
        self.Datetime = datetime.datetime.now()
        self.Flag = "!"
        self.Payee = "Unknown"
        self.Description = "Unknown"
        self.Tags = []
        self.Metadata = []
        self.MetadataDict = {}
        self.Postings = []
        self.Raw = []
    
    # Sets the Datetime of this Entry to a parsed representation of the string provided.
    # Supports date strings in the following formats: %m/%d/%Y (e.g. 05/02/2018)
    def setDate(self, string):
        self.Datetime = datetime.datetime.strptime(string, "%m/%d/%Y")
    
    # TODO sets the Datetime of this Entry to the parsed representation of the string provided
    # as well as includes the time in the user's locale. If no time part could be parsed, then
    # 0:00:00 in the user locale's timezone is assumed.
    # Since Beancount does not store time as a first class field, we store it as a metadata
    # key-value using the 'time' key.
    def setDatetime(self, string):
        pass

    def setFlag(self, string):
        self.Flag = string
    
    def setPayee(self, string):
        self.Payee = string
    
    def setDescription(self, string):
        self.Description = string
    
    def addTags(self, string):
        self.Tags.append(string)

    def addMetadata(self, metadata):
        if isinstance(metadata, BeancountMetadata):
            self.Metadata.append(metadata)
            self.MetadataDict[metadata.Key] = metadata.Value
        else:
            raise AttributeError()
    
    def getMetadataValueFor(self, key):
        if key in self.MetadataDict.keys():
            return self.MetadataDict[key]
        else:
            return None
    
    def hasMetadata(self, key):
        return key in self.MetadataDict.keys()

    def addPosting(self, posting):
        self.Postings.append(posting)

    def removePosting(self, index):
        raise Exception("Not Implemented!")

    # Removes all postings from this entry
    def clearPostings(self):
        self.Postings = []

    # Validates the postings of this entry. A valid entry means that the sum
    # of all the postings is equal to zero! (The beauty of double-entry accounting!)
    def validatePostings(self):
        sum = 0
        for posting in self.Postings:
            sum += posting.AmountValue
        if sum != 0:
            return False
        return True
    
    # NOT IDEAL
    # This should only be used as a temporary measure, while we're working on
    # getting parsing of all the other data (posting/metadata) worked out.
    @DeprecationWarning
    def addRaw(self, raw):
        self.Raw.append(raw)

    def toBeancountStr(self):
        """ Canonical function for rendering the text form of this BeancountEntry. """
        dateStr = self.Datetime.strftime("%Y-%m-%d")
        beancountStr = dateStr + " " + self.Flag + " \"" + self.Payee + "\" \"" + self.Description + "\""
        for tag in self.Tags:
            beancountStr += " " + tag
        for metadata in self.Metadata:
            beancountStr += "\n"
            beancountStr += "  " + metadata.toBeancountStr()
        for posting in self.Postings:
            beancountStr += "\n"
            beancountStr += "  " + posting.toBeancountStr()
        for raw in self.Raw:
            beancountStr += "\n"
            beancountStr += "  " + raw
        beancountStr += "\n"

        return beancountStr
    
    def __str__(self):
        return self.toBeancountStr()
    def __repr__(self):
        return self.toBeancountStr()

# TODO: migrate this to pytest unit testing code
if __name__ == "__main__":
    print()
    print("--- Test ---")
    print("Test: what does it look like when we make a beancount string?")
    mock = BeancountEntry()
    mock.addPosting(BeancountPosting())
    mock.addPosting(BeancountPosting())
    print(mock.toBeancountStr())

    print()
    print("--- Test ---")
    print("Test: can postings be validated?")
    print("\tSub-test: expected True, got: " + str(mock.validatePostings()))
    mock.clearPostings()
    mock.addPosting(BeancountPosting(amountValue=1.00))
    mock.addPosting(BeancountPosting(amountValue=2.00))
    print("\tSub-test: expected False, got: " + str(mock.validatePostings()))

    print()
    print("--- Test ---")
    print("Test: can we parse dates of the form month/day/year?")
    mock = BeancountEntry()
    mock.setDate("05/01/2018")
    print(mock)

    print()
    print("--- Test ---")
    print("Test: tags can be added to a BeancountEntry")
    mock.addTags("^foo")
    mock.addTags("^bar")
    print(mock)

    print("All tests have finished.")
