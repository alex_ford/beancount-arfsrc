#!/usr/env python3

from .beancount_directive import BeancountDirective

class BeancountTransaction(BeancountDirective):

    def __init__(self, flag="!", payee="Payee", desc="Description"):
        super.__init__()
