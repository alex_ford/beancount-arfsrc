#!/usr/env python3

""" Simple Filer.
The most primitive Filer of Beancount directives. This Filer simply appends
directives to the end of a singular data file. No sorting and no special
organization is done.
"""

from . import ABCFiler
from beancount_arfsrc.directive import BeancountDirective

class SimpleFiler(ABCFiler):

    def __init__(self):
        pass
    
    def save(self, directive: BeancountDirective):
        pass
