#!/usr/env python3

"""
Author: Alex Richard Ford (arf4188@gmail.com)
"""

import logging
logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)

from . import ABCFiler
from beancount_arfsrc.directive import BeancountDirective

import os
import arrow

class ARFComplexFiler(ABCFiler):
    """ ARF Complex Filer.
    This Filer implements my own strategy for organization of Beancount directives:

    - A "data repo" (backed by Git) is used for all data files
    - Transaction Directives are stored under ${DATA_DIR}/transactions/
        - They are further sorted into folders by year: ${DATA_DIR}/transactions/YYYY
        - Then placed into files organized by "YYYYMM.beancount"
    - Other Directives are not yet implemented.
        - With that said, I do have a specification for them...
    """

    DATA_DIR = os.path.expanduser("~/Workspaces-Personal/beancount/beancount-data")
    TRANSACTIONS_DIR = os.path.join(DATA_DIR, "transactions")

    def __init__(self):
        self._check_dirs()
    
    def _check_dirs(self):
        assert os.path.exists(self.DATA_DIR)
        assert os.path.exists(self.TRANSACTIONS_DIR)

    def save(self, directive: BeancountDirective):
        _log.info("")
        _log.info("{}".format(self.DATA_DIR))
        _log.info("{}".format(self.TRANSACTIONS_DIR))
