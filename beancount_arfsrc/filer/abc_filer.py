#!/usr/env python3

""" Abstract Base Class Filer.
This is the super class for Filer classes which determine how to organize
Beancount data files. The most simple Filer is one which simply inserts
directives into a single file. (See SimpleFiler in simple_filer.py)

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License
"""

from beancount_arfsrc.directive import BeancountDirective

class ABCFiler():

    def __init__(self):
        pass
    
    def save(self, directive: BeancountDirective):
        raise UnimplementedException()

