#!/usr/env python

from setuptools import find_packages, setup

setup(name='beancount_arfsrc',
      version='0.3.0',
      description='Beancount ARF Source Library',
      author='Alex Richard Ford',
      author_email='arf4188@gmail.com',
      url='http://www.alexrichardford.com/',
      packages=find_packages()
)
